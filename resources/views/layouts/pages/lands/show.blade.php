
@extends('layouts.main')

@section('content')
    <div class="card">
        <div class="card-header">

            <p class="card-title"><span style="font-weight: 900">{{$lands['libelle']}}</span> <small class="m-l-sm">{{$lands['code_indicatif']}}</small></p>

            <div class="card-tools">
                <button type="button" class="btn btn-tool" data-card-widget="collapse" title="Collapse">
                    <i class="fas fa-minus"></i>
                </button>
                <button type="button" class="btn btn-tool" data-card-widget="remove" title="Remove">
                    <i class="fas fa-times"></i>
                </button>
            </div>
        </div>
        <div class="card-body">
            <div><span style="font-weight: 700">Continent : </span>{{$lands['continent']}}</div>
            <div><span style="font-weight: 700">Population : </span>{{$lands['population']}}</div>
            <div><span style="font-weight: 700">Capitale : </span>{{$lands['capitale']}}</div>
            <div><span style="font-weight: 700">Monnaie : </span>{{$lands['monnaie']}}</div>
            <div><span style="font-weight: 700">Langue : </span>{{$lands['langue']}}</div>
            <div><span style="font-weight: 700">Superficie : </span>{{$lands['superficie']}}</div>
            <div><span style="font-weight: 700">Est laique : </span>{{($lands['est_laique'])}}</div>
        </div>
        <!-- /.card-body -->
        <div class="card-footer">
            {{$lands['description']}}
        </div>
        <!-- /.card-footer-->
    </div>
@endsection
