
@extends('layouts.main')

@section('content')
    <div class="card card-primary">
        <div class="card-header">
            <h3 class="card-title">Formulaire de modification des pays</h3>
        </div>
        <!-- /.card-header -->
        @if ($errors->any())
            <div class="alert alert-danger">
                <ul>
                    @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div>
        @endif
        <!-- form start -->
        <form action="{{ route('lands.update', ["id" => $lands->id]) }}" method="POST">
            @csrf
            @method('POST')
            <div class="card-body">
                <div class="form-group">
                    <label for="exampleInputEmail1">Libelle</label>
                    <input type="text" name="libelle" class="form-control" id="exampleInputEmail1"
                        placeholder="Entrez le libelle..." value="{{ $lands['libelle'] }}">
                </div>
                <div class="form-group">
                    <label for="exampleInputEmail1">Code indicatif</label>
                    <input type="text" name="code_indicatif" class="form-control" id="exampleInputEmail1"
                        placeholder="Entrez le code indicatif précédé d'un +..." value="{{ $lands['code_indicatif'] }}">
                </div>
                <div class="form-group">
                    <label for="exampleInputEmail1">Continent</label>
                    <select name="continent" id="" value="{{ $lands['continent'] }}">
                        <option value="Afrique">Afrique</option>
                        <option value="Amérique">Amérique</option>
                        <option value="Europe">Europe</option>
                        <option value="Asie">Asie</option>
                        <option value="Océanie">Océanie</option>
                        <option value="Antartique">Antartique</option>
                    </select>
                </div>
                <div class="form-group">
                    <label for="exampleInputEmail1">Population</label>
                    <input type="text" name="population" class="form-control" id="exampleInputEmail1"
                        placeholder="Entrez la population ..." value="{{ $lands['population'] }}">
                </div>
                <div class="form-group">
                    <label for="exampleInputEmail1">Capitale</label>
                    <input type="text" name="capitale" class="form-control" id="exampleInputEmail1"
                        placeholder="Entrez la capitale..." value="{{ $lands['capitale'] }}">
                </div>
                <div class="form-group">
                    <label for="exampleInputEmail1">Monnaie</label>
                    <select name="monnaie" id="" value="{{ $lands['monnaie'] }}">
                        <option value="XOF">XOF</option>
                        <option value="EUR">EUR</option>
                        <option value="DOLLAR">DOLLAR</option>
                    </select>
                </div>
                <div class="form-group">
                    <label for="exampleInputEmail1">Langue</label>
                    <select name="langue" id="" value="{{ $lands['langue'] }}">
                        <option value="FR">Francais</option>
                        <option value="EN">Anglais</option>
                        <option value="AR">Arabe</option>
                        <option value="ES">Espagnol</option>
                    </select>
                </div>
                <div class="form-group">
                    <label for="exampleInputEmail1">Superficie</label>
                    <input type="text" name="superficie" class="form-control" id="exampleInputEmail1"
                        placeholder="Entrez la superficie (km^2)..." value="{{ $lands['superficie'] }}">
                </div>
                <div class="form-group">
                    <label for="exampleInputEmail1">Est laique ?</label>
                    <select name="est_laique" id="" value="{{ $lands['est_laique'] }}">
                        <option value="1">OUI</option>
                        <option value="0">NON</option>
                    </select>
                </div>
                <div class="form-group">
                    <label for="exampleInputPassword1">Description</label>
                    <textarea name="description" class="form-control" id="exampleInputPassword1"
                        placeholder="Entrez la description...">{{ $lands['description'] }}</textarea>
                </div>
            </div>
            <!-- /.card-body -->
            <div class="card-footer">
                <button type="submit" class="btn btn-primary">Modifier</button>
                <a href=" " class="btn btn-default float-right" style="background-color: red">Annuler</a>
            </div>
        </form>
    </div>
@endsection
