@extends('layouts.main')

@section('content')
<div class="row">
    <div class="col-12">
      <div class="card">
        <div class="card-header">
          <h3 class="card-title"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">Liste des pays</font></font></h3>

          <div class="card-tools">
            <div class="input-group input-group-sm" style="width: 150px;">
                <a href="{{ route('lands.create')}}">
                    <button class="btn btn-success">Ajout des pays</button>
                </a>
            </div>
          </div>
        </div>
        <!-- /.card-header -->
        <div class="card-body table-responsive p-0">
          <table class="table table-hover text-nowrap">
            <thead>
              <tr>
                <th><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">Id</font></font></th>
                <th><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">Libéllé</font></font></th>
                <th><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">Description</font></font></th>
                <th><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">Code_Indicatif</font></font></th>
                <th><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">Continent</font></font></th>
                <th><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">Population</font></font></th>
                <th><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">Capitale</font></font></th>
                <th><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">Monnaie</font></font></th>
                <th><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">Langue</font></font></th>
                <th><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">Superficie</font></font></th>
                <th><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">Est_Laique</font></font></th>
                <th><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">Options</font></font></th>

              </tr>
            </thead>
            <tbody>
                @foreach($lands as $land)
                    <tr>
                        <td><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">{{$land->id}}</font></font></td>
                        <td><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">{{$land->libelle}}</font></font></td>
                        <td><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">{{$land->description}}</font></font></td>
                        <td><span class="tag tag-success"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">{{$land->code_indicatif}}</font></font></span></td>
                        <td><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">{{$land->continent}}</font></font></td>
                        <td><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">{{$land->population}}</font></font></td>
                        <td><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">{{$land->capitale}}</font></font></td>
                        <td><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">{{$land->monnaie}}</font></font></td>
                        <td><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">{{$land->langue}}</font></font></td>
                        <td><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">{{$land->superficie}}</font></font></td>
                        <td><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">{{$land->est_laique}}</font></font></td>
                        <td>
                            <a href="{{ route('lands.show',['id'=> $land->id])}}" class="btn btn-warning btn-sm" title="Voir"><i class="nav-icon fas fa-eye"></i></a>
                            <a href="{{ route('lands.edit',['id'=> $land->id])}}" class="btn btn-primary btn-sm" title="Modifier"><i class="nav-icon fas fa-edit"></i></a>
                            <a href="{{ route('lands.delete',['id'=> $land->id])}}" class="btn btn-danger btn-sm" title="Supprimer"><i class="nav-icon fas fa-trash"></i></a>
                        </td>
                    </tr>
                @endforeach
            </tbody>
          </table>
        </div>
        <!-- /.card-body -->
      </div>
      <!-- /.card -->
    </div>
  </div>
@endsection
