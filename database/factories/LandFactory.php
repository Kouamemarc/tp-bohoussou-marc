<?php

namespace Database\Factories;

use App\Models\Land;
use Illuminate\Database\Eloquent\Factories\Factory;

/**
 * @extends \Illuminate\Database\Eloquent\Factories\Factory<\App\Models\Land>
 */
class LandFactory extends Factory
{
    /**
     *The name of the factory's corresponding model.

     * @var string
     */

    protected $model = Land::class;

    /**
     * Define the model's default state.


     * @return array
     */

    public function definition()
    {
        return [

            "libelle" =>$this->faker->word(),
            "description" =>$this->faker->paragraph(),
            "code_indicatif" =>$this->faker->unique()->numerify('+###'),
            "continent" =>$this->faker->randomElement(["Afrique","Asie","Europe","Amerique","Oceanie","Antartique"]),
            "population" =>$this->faker->numberBetween(),
            "capitale" =>$this->faker->city(),
            "monnaie" =>$this->faker->randomElement(["XOF","EUR","DOLLAR"]),
            "langue" =>$this->faker->randomElement(["FR","EN","AR","ES"]),
            "superficie" =>$this->faker->numberBetween(),
            "est_laique" =>$this->faker->boolean(),

        ];
    }
}
