<?php

namespace App\Http\Controllers;

use App\Models\Land;
use Illuminate\Http\Request;

class LandController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $lands = Land::all();
        return view('layouts.pages.lands.index', compact('lands'));//
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('layouts.pages.lands.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            "libelle"=> "required",
            "capitale"=> "required",
            "monnaie"=> "required",
            "description"=> "required",
            "code_indicatif"=> "required",
            "continent"=> "required",
            "population"=> "required",
            "langue"=> "required",
            "superficie"=> "required",
            "est_laique"=> "required"

        ]);

         //Enregistrement dans la bd
        Land::create([


            "libelle" =>$request->get('libelle'),
            "description"=> $request->get('description'),
            "code_indicatif"=>$request->get('code_indicatif'),
            "continent" =>$request->get('continent'),
            "population" =>$request->get('population'),
            "capitale" =>$request->get('capitale'),
            "monnaie" =>$request->get('monnaie'),
            "langue" =>$request->get('langue'),
            "superficie" =>$request->get('superficie'),
            "est_laique" =>$request->get('est_laique')
        ]);

        return redirect()->route("lands.index");

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $lands = Land::findOrFail($id);
        return view ("layouts.pages.lands.show", ["lands" => $lands]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $lands = Land::findOrFail($id);
        return view ("layouts.pages.lands.edit", ["lands" => $lands]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
       $validatedData = $request->validate([
        "libelle"=> "required",
        "capitale"=> "required",
        "monnaie"=> "required",
        "description"=> "required",
        "code_indicatif"=> "required",
        "continent"=> "required",
        "population"=> "required",
        "langue"=> "required",
        "superficie"=> "required",
        "est_laique"=> "required"

       ]);
       Land::whereId($id)->update($validatedData);
       //$lands->libelle = $request->libelle;
       return redirect ('/lands')->with(['Succès'=> 'Liste des pays mis à jour!!']);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $lands = Land ::findOrFail($id);
        $lands->delete();
        return redirect()->route('lands.index')->with(['Succès'=> 'Suppression effectuée!!']);
    }
}

